﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject player;
    [SerializeField] private string roomName = "default";
    [SerializeField] private string playerName = "default";

    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.LocalPlayer.NickName = playerName;
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log($"NM.16: Connected to Server [ {PhotonNetwork.CloudRegion} ]");

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 20;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;

        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
    }

    // public override void OnJoinRandomFailed(short returnCode, string message)
    // {
    //     Debug.Log($"Failed to join room. Error: [ Code : {returnCode}, Reason : {message} ]");
    // }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log($"Failed to join room. Error: [ Code : {returnCode}, Reason : {message} ]");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log($"Player [Player] has joined!");

        foreach (var item in PhotonNetwork.PlayerList)
        { 
            Debug.Log($"Player list: [{ item.NickName }]");
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log($"Player [{newPlayer.NickName}] has joined!");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
